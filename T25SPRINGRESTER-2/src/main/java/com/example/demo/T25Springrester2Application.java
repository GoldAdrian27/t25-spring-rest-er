package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T25Springrester2Application {

	public static void main(String[] args) {
		SpringApplication.run(T25Springrester2Application.class, args);
	}

}
