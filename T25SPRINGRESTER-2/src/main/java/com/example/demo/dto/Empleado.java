package com.example.demo.dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="empleados")

public class Empleado {
	//Atributos de entidad cliente
			//@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
			@Id		
			private String dni;
			@Column(name = "nombre")//no hace falta si se llama igual
			private String nombre;
			@Column(name = "apellido")//no hace falta si se llama igual
			private String apellido;

			@ManyToOne
			@JoinColumn(name = "departamento")
			private Departamento departamento;

			
			public Empleado() {
				
			}
			
			public Empleado(String dni, String nombre, String apellido, Departamento departamento) {
				super();
				this.dni = dni;
				this.nombre = nombre;
				this.apellido = apellido;
				this.departamento = departamento;
			}

			public String getDNI() {
				return dni;
			}

			public void setDNI(String dNI) {
				dni = dNI;
			}

			public String getNombre() {
				return nombre;
			}

			public void setNombre(String nombre) {
				this.nombre = nombre;
			}

			public String getApellido() {
				return apellido;
			}

			public void setApellido(String apellido) {
				this.apellido = apellido;
			}

			public Departamento getDepartamento() {
				return departamento;
			}

			public void setDepartamento(Departamento departamento) {
				this.departamento = departamento;
			}
			
			
			
}
