package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Departamento;


public interface IDepartamentoServiceImpl {
	//Metodos del CRUD
	public List<Departamento> listarDepartamentos(); //Listar All 
	
	public Departamento guardarDepartamento(Departamento departamento);	//Guarda un Articulo CREATE
	
	public Departamento departamentoXID(Long id); //Leer datos de un Articulo READ
	
	public Departamento actualizarDepartamento(Departamento departamento); //Actualiza datos del Articulo UPDATE
	
	public void eliminarDepartamento(Long id);// Elimina el Articulo DELETE
}
