DROP table IF EXISTS `empleados`;
DROP TABLE IF EXISTS `departamentos`;


CREATE TABLE `departamentos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `presupuesto` int DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);

CREATE TABLE `empleados` (
  `DNI` varchar(8) NOT NULL,
    PRIMARY KEY (`DNI`),
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `departamento` int(11) NOT NULL,
  CONSTRAINT `departamento_fk` FOREIGN KEY (`departamento`) REFERENCES `departamentos` (`codigo`)
);


insert into departamentos (codigo, nombre, presupuesto) values (1, 'departamentos 1', 400);
insert into departamentos (codigo, nombre, presupuesto) values (2, 'departamentos 2', 300);
insert into departamentos (codigo, nombre, presupuesto) values (3, 'departamentos 3', 350);
insert into departamentos (codigo, nombre, presupuesto) values (4, 'departamentos 4', 370);
insert into departamentos (codigo, nombre, presupuesto) values (5, 'departamentos 5', 310);


insert into empleados (DNI, nombre, apellido, departamento) values ('121312F','nombre 1', 'apellido 1', 1);
insert into empleados (DNI, nombre, apellido, departamento) values ('221312F','nombre 2', 'apellido 2', 1);
insert into empleados (DNI, nombre, apellido, departamento) values ('321312F','nombre 3', 'apellido 3', 1);
insert into empleados (DNI, nombre, apellido, departamento) values ('421312F','nombre 4', 'apellido 4', 1);
insert into empleados (DNI, nombre, apellido, departamento) values ('521312F','nombre 5', 'apellido 5', 1);
