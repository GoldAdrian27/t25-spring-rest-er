DROP table IF EXISTS `articulos`;
DROP TABLE IF EXISTS `fabricantes`;


CREATE TABLE `fabricantes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);

CREATE TABLE `articulos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `precio` varchar(250) DEFAULT NULL,
  `fabricante` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  CONSTRAINT `fabricantes_fk` FOREIGN KEY (`fabricante`) REFERENCES `fabricantes` (`codigo`)
);


insert into fabricantes (codigo, nombre) values (1, 'fabricante 1');
insert into fabricantes (codigo, nombre) values (2, 'fabricante 2');
insert into fabricantes (codigo, nombre) values (3, 'fabricante 3');
insert into fabricantes (codigo, nombre) values (4, 'fabricante 4');
insert into fabricantes (codigo, nombre) values (5, 'fabricante 5');


insert into articulos (codigo, nombre, precio, fabricante) values (1,'articulos 1', '300€', 1);
insert into articulos (codigo, nombre, precio, fabricante) values (2,'articulos 2', '200€', 1);
insert into articulos (codigo, nombre, precio, fabricante) values (3,'articulos 3', '100€', 1);
insert into articulos (codigo, nombre, precio, fabricante) values (4,'articulos 4', '400€', 1);
insert into articulos (codigo, nombre, precio, fabricante) values (5,'articulos 5', '500€', 1);
