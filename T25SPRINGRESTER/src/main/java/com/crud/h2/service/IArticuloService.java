package com.crud.h2.service;

import java.util.List;

import com.crud.h2.dto.Articulo;

public interface IArticuloService {

	//Metodos del CRUD
		public List<Articulo> listarArticulos(); //Listar All 
		
		public Articulo guardarArticulo(Articulo Articulo);	//Guarda un Articulo CREATE
		
		public Articulo articuloXID(Long id); //Leer datos de un Articulo READ
		
		public Articulo actualizarArticulo(Articulo Articulo); //Actualiza datos del Articulo UPDATE
		
		public void eliminarArticulo(Long id);// Elimina el Articulo DELETE
}
