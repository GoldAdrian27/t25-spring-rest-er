package com.crud.h2.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="fabricantes")//en caso que la tabla sea diferente
public class Fabricante  {
 
	//Atributos de entidad fabricante
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	private Long codigo;
	@Column(name = "nombre")//no hace falta si se llama igual
	private String nombre;

    @OneToMany
    @JoinColumn(name="codigo")
    private List<Articulo> articulo;
	
	//Constructores
	
	public Fabricante() {
	
	}

	/**
	 * @param id
	 * @param nombre
	 */
	
	public Fabricante(Long codigo, String nombre) {
		//super();
		this.codigo = codigo;
		this.nombre = nombre;
	}
	
	//Getters y Setters
	
	/**
	 * @return the id
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param id the id to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	/**
	 * @return the Articulo
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "articulo")
	public List<Articulo> getArticulo() {
		return articulo;
	}

	/**
	 * @param Articulo the Articulo to set
	 */
	public void setArticulo(List<Articulo> articulo) {
		this.articulo = articulo;
	}

	//Metodo impresion de datos por consola
	@Override
	public String toString() {
		return "fabricante [codigo=" + codigo + ", nombre=" + nombre + "]";
	}
	
}
