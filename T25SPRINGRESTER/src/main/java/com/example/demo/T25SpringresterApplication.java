package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T25SpringresterApplication {

	public static void main(String[] args) {
		SpringApplication.run(T25SpringresterApplication.class, args);
	}

}
